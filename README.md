# KNN using OpenCv and sklearn with Python

## References
- [OpenCv tutorial](https://docs.opencv.org/4.x/d8/d4b/tutorial_py_knn_opencv.html)
- [sklearn KNN](https://scikit-learn.org/stable/modules/generated/sklearn.neighbors.KNeighborsClassifier.html)